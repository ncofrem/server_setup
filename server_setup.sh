#!/bin/bash
echo ""
echo "POS SERVER SETUP SCRIPT"
echo ""
echo ""

echo "Checking for admin privileges..."
if [ "$EUID" -ne 0 ]; then
  echo "You need admin privileges to execute this :("
  exit -1
else
  echo "Admin privileges found :D"
fi

if [ ! -f profile.ovpn ]; then
  echo "No VPN connection file found :("
  exit -1
fi

echo "Reading variables from variables.sh..."
if [ -f variables.sh ]; then
  echo "Variables found :D"
else
  echo "File variables.sh not found :("
  exit -1
fi

echo "Installing dependencies"
sudo apt-get remove docker docker-engine docker.io
sudo apt update
sudo apt-get install --install-recommends linux-generic-hwe-16.04
sudo apt install -y ufw isc-dhcp-server hostapd midori openvpn
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get -y install docker-ce
sudo groupadd docker
sudo usermod -aG docker $USER
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo "Sourcing configured variables..."
source variables.sh
echo "- INTERFACES"
echo "  - EXTERNAL_INTERFACE: $EXTERNAL_INTERFACE"
echo "  - INTERNAL_WIFI_INTERFACE: $INTERNAL_WIFI_INTERFACE"
echo "  - INTERNAL_ETHERNET_INTERFACE: $INTERNAL_ETHERNET_INTERFACE"
echo ""
echo ""

bash generator_interfaces.sh
echo "- ARCHIVO 'interfaces' GENERADO"

bash generator_hostapd_conf.sh
echo "- ARCHIVO 'hostapd.conf' GENERADO"

bash generator_isc_dhcp_server.sh
echo "- ARCHIVO 'isc-dhcp-server' GENERADO"

bash generator_dhcpd_conf.sh
echo "- ARCHIVO 'dhcpd.conf' GENERADO"

echo "- CARGANDO NAT SCRIPT"
bash generator_nat_script.sh

bash generator_openvpn.sh
