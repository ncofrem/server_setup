#!/bin/bash

echo "Configuring OpenVPN..."
sudo cp profile.ovpn /etc/openvpn/profile.conf
if [ -f /etc/openvpn/profile.conf ]; then
  echo "OpenVPN configured.."
else
  echo "Failed to create OpenVPN configuration :("
  exit -1
fi