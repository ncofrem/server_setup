#!/bin/bash
source variables.sh

DIR=/etc/default/isc-dhcp-server
FILENAME=isc-dhcp-server

if [ -e $FILENAME ]
then
  rm -f $FILENAME
fi

echo "INTERFACES=\"$INTERNAL_WIFI_INTERFACE $INTERNAL_ETHERNET_INTERFACE\"
" >> $FILENAME

yes | cp -rf $FILENAME $DIR

rm -f $FILENAME
