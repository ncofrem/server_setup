#!/bin/bash
source variables.sh

DIR=/etc/dhcp
FILENAME=dhcpd.conf

if [ -e $FILENAME ]
then
  rm -f $FILENAME
fi

echo "ddns-update-style none;

option domain-name \"nnodes.local\";
option domain-name-servers 1.1.1.1, 8.8.8.8, 1.0.0.1, 8.8.4.4;

default-lease-time 600;
max-lease-time 7200;

log-facility local7;

subnet 172.30.30.0 netmask 255.255.255.0 {
	range 172.30.30.20 172.30.30.220;
	option subnet-mask 255.255.255.0;
	option broadcast-address 172.30.30.255;
	option routers 172.30.30.1;
}

subnet 172.25.25.0 netmask 255.255.255.0 {
	range 172.25.25.20 172.25.25.220;
	option subnet-mask 255.255.255.0;
	option broadcast-address 172.25.25.255;
	option routers 172.25.25.1;
}
" >> $FILENAME

yes | cp -rf $FILENAME $DIR

rm -f $FILENAME
