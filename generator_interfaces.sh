#!/bin/bash
source variables.sh

DIR=/etc/network/interfaces
FILENAME=interfaces

if [ -e $FILENAME ]
then
  rm -f $FILENAME
fi

echo "# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto $EXTERNAL_INTERFACE
iface $EXTERNAL_INTERFACE inet dhcp

auto $INTERNAL_WIFI_INTERFACE
iface $INTERNAL_WIFI_INTERFACE inet static
        hostapd /etc/hostapd/hostapd.conf
        address 172.30.30.1
        network 172.30.30.0
        netmask 255.255.255.0
        broadcast 172.30.30.255

auto $INTERNAL_ETHERNET_INTERFACE
allow-hotplug $INTERNAL_ETHERNET_INTERFACE
iface $INTERNAL_ETHERNET_INTERFACE inet static
        address 172.25.25.1
        network 172.25.25.0
        netmask 255.255.255.0
        broadcast 172.25.25.255
" >> $FILENAME

yes | cp -rf $FILENAME $DIR

rm -f $FILENAME
